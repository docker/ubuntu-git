FROM debian:bookworm-slim

RUN apt update && \
   apt install --yes curl docker git && \
   rm -rf /var/lib/apt/lists/*
